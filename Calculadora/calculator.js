/**
 * Calculo ancho de banda recomendado de bajada
 */
function aBRB(){
    var a = document.calcul.vTv.value;
    var b = document.calcul.vPh.value;
    var c = document.calcul.vCj.value;
    var d = document.calcul.vPp.value;
    var e = document.calcul.vVc.value;
    try{
        a = (isNaN(parseFloat(a)))? 0 : parseFloat(a);
        b = (isNaN(parseFloat(b)))? 0 : parseFloat(b);
        c = (isNaN(parseFloat(c)))? 0 : parseFloat(c);
        d = (isNaN(parseFloat(d)))? 0 : parseFloat(d);
        e = (isNaN(parseFloat(e)))? 0 : parseFloat(e);
        
        document.calcul.vBb.value = (a * 5) + (b * 2) + (c * 7) + (Math.max(d * 5, e * 5));
        
    }catch(z){}
}
/**
 * Calculo de ando de banda recomendado de subida
 */
function aBRS(){
    var g = document.calcul.vCa.value;
    var e = document.calcul.vVc.value;
    var c = document.calcul.vCj.value;
    try {
        g = (isNaN(parseInt(g)))? 0 : parseInt(g); 
        e = (isNaN(parseFloat(e)))? 0 : parseFloat(e);
        c = (isNaN(parseFloat(c)))? 0 : parseFloat(c);

        document.calcul.vBs.value = e * 4 + g * 5 + c * 3;
    }catch(z){}
}
/**
 * Calculo de Cantidad de Access Point recomendados
 */
function cAPR(){
    var i = document.calcul.vSc.value;
    try{
        i = (isNaN(parseInt(i)))? 0 : parseInt(i);

        document.calcul.tCAPR.value = Math.round(i / 130) ;
    }catch(z){}
}
/**
 * ?El cursor debe pasar por el campo para escuchar el evento onKeyUp
 * Resultados ABB
 */
function totalABB(){
    var f = document.calcul.vBb.value;
    try{
        f = (isNaN(parseInt(f)))? 0 : parseInt(f);

        document.calcul.tABB.value = Math.round(f / 10) * 10;
    }catch(z){}
}
/**
 * ?El cursor debe pasar por el campo para escuchar el evento onKeyUp
 * Resultados ABS
 */
function totalABS(){
    var h = document.calcul.vBs.value;
    try{
        h = (isNaN(parseInt(h)))? 0 : parseInt(h);

        document.calcul.tABS.value = Math.round(h / 5) * 5;
    }catch(z){}
}
/**
 * ?El cursor debe pasar por el campo para escuchar el evento onKeyUp
 * Resultados Plan Recomendado Mbps
 */
function totalPlan(){
    var j = document.calcul.tABB.value;
    var k = document.calcul.tABS.value;
    try{
        j = (isNaN(parseInt(j)))? 0 : parseInt(j);
        k = (isNaN(parseInt(k)))? 0 : parseInt(k);

        document.calcul.tPRC.value = Math.max(j, k * 2);
    }catch(z){}
}
/**
 * ?El cursor debe pasar por el campo para escuchar el evento onKeyUp
 * Resultados GPON
 */
function totalGPON(){
    var l = document.calcul.tPRC.value;
    try{
        l = (isNaN(parseInt(l)))? 0 : parseInt(l);
        document.calcul.tGPON.value = "";
        if(l <= 60){
            document.calcul.tGPON.value = "X";
        }
    }catch(z){}
}
/**
 * ?El cursor debe pasar por el campo para escuchar el evento onKeyUp
 * Resultados VDSL
 */
function totalVDSL(){
    var l = document.calcul.tPRC.value;
    try{
        l = (isNaN(parseInt(l)))? 0 : parseInt(l);
        document.calcul.tVDSL.value = "";
        if(l <= 60){
            document.calcul.tVDSL.value = "X";
        }
    }catch(z){}
}
/**
 * ?El cursor debe pasar por el campo para escuchar el evento onKeyUp
 * Resultados ADSL
 */
function totalADSL(){
    var l = document.calcul.tPRC.value;
    try{
        l = (isNaN(parseInt(l)))? 0 : parseInt(l);
        document.calcul.tADSL.value = "";
        if(l <= 10){
            document.calcul.tADSL.value = "X";
        }
    }catch(z){}
}